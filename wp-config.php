<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%;!uS0<DNJ#Mx5_p0t(AJzf4;}e!Q@|y0cCVw2`r2#xNr%z@=XCr1Q=87!R9?xA!');
define('SECURE_AUTH_KEY',  '|3<*^8pC;Di,X&}VK<y ju;>@g!Z0@M5Q~~{6U`dR|x9hQjMdw@Sxg&l,)?(4blh');
define('LOGGED_IN_KEY',    'UG)Wlkl<,rfa5jq}`@.M9?#x#Y6ESQZT~[y05AJZ_pgt8fo:Nx;u B7qfI+CN`-p');
define('NONCE_KEY',        '>D5PTc!:0bS2[)P8vhFTC]Bq;=!LqVn56bAx&C51$;&`5:ZJKs?x2zW&O26coo3m');
define('AUTH_SALT',        'Hg28)=,}B+$Jc294/AGk$t.w]Bx/o[X,w$E*0pnkK_Yx)6;nw1d:z!2T>yQ-Ytkh');
define('SECURE_AUTH_SALT', 'z=-cga)&U9wJ=cTh&&Q>}Z{GGrw!0=Y#D_yo{$<f!7w{gvrBG5>U+bqvEJt,!{bm');
define('LOGGED_IN_SALT',   '>2U8lLyc%Adro$9*(SW0J@5q4C~DaJLR9<=M!5W@ts)>@#gTV)W}+F-)zI)rmt^D');
define('NONCE_SALT',       'q-!e}NX{CpgDf3ux%^x}J9]`K78T 9XH5(bdOZ^OvyS/piuRaO`o_@jDA6s;~KAF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
