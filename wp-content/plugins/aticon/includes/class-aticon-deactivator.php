<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.linkedin.com/in/zhekaq
 * @since      1.0.0
 *
 * @package    Aticon
 * @subpackage Aticon/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Aticon
 * @subpackage Aticon/includes
 * @author     zhekaq <zhekaq20@gmail.com>
 */
class Aticon_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
