<?php

/**
 * Fired during plugin activation
 *
 * @link       www.linkedin.com/in/zhekaq
 * @since      1.0.0
 *
 * @package    Aticon
 * @subpackage Aticon/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Aticon
 * @subpackage Aticon/includes
 * @author     zhekaq <zhekaq20@gmail.com>
 */
class Aticon_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
